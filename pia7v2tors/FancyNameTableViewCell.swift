//
//  FancyNameTableViewCell.swift
//  pia7v2tors
//
//  Created by Bill Martensson on 2017-09-07.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class FancyNameTableViewCell: UITableViewCell {

    @IBOutlet weak var niceNameLabel: UILabel!
    
    @IBOutlet weak var niceNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
