//
//  ViewController.swift
//  pia7v2tors
//
//  Created by Bill Martensson on 2017-09-07.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // Titta vad vi ändrar
    // En till kommentar
    
    var namnen = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        namnen.append("Arne")
        namnen.append("Bertil")
        namnen.append("Caesar")
        namnen.append("David")
        namnen.append("Erik")
        namnen.append("Fredrik")
        namnen.append("Göran")
        namnen.append("Henrik")
        namnen.append("Ingvar")
        namnen.append("Johan")
        namnen.append("Kalle")

        print(namnen.count)
        print(namnen[1])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namnen.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "finaraden") as! FancyNameTableViewCell
        
        print("VI SKAPADE EN RAD!!! \(indexPath.row)  ")
        
        //cell.textLabel?.text = namnen[indexPath.row]
        
        cell.niceNameLabel.text = namnen[indexPath.row]
        
        cell.niceNumberLabel.text = String(indexPath.row)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("TRYCK PÅ RAD!!!")
        print(namnen[indexPath.row])
    }
    
    
}

